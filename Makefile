all: 
	docker run -v ${PWD}:/resume moderncv

configure: 
	docker build -t moderncv .

clean: 
	rm -f *.aux *.log *.pdf *.out