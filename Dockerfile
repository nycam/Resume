FROM ubuntu:16.04 

RUN apt-get update && apt-get install -y \
    texlive-xetex \
    texlive-fonts-extra

WORKDIR /resume

CMD echo | xelatex Resume\ -\ Baptiste\ Prunot.tex