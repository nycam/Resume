My Personnal Resume
====================

Written in latex using modercv.
It is translated in French and English, you just have to uncomment the corresponding line at the beginning of the file and compile the document :slightly_smiling_face:

## Build
To simplify the setup of latex I have added a docker image.
use `make configure` to setup a container with `tex-live` installed.

To build the resume you can use `make`

## Switching from French to english
Uncomment/comment the following lines in the main file
```latex
    %
    % SET LANGUAGE 
    %
    %\entrue%
    \frtrue%
```
